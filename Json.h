#include <iostream>
#include <string>
#include <map>
#include <vector>
using namespace std;
#include "Variables.h"


variable dummy_var;
std::map<std::string, variable> varmap;
std::vector<variable> vec;

#define CONCAT(a, b) a ## b
#define NEWVAR(Var) CONCAT(Var, __COUNTER__ )
#define PROGRAM_BEGIN int main(){
#define OBJECT   (*new variable(object)) = *new std::map < std::string, variable > {


#define KEY(key_name) }, { #key_name  , false?   (*new variable(object))                 // to metatrepei se "key_name"
#define ARRAY (*new variable(jarray))
#define TRUE (*new variable((bool)true, boolean))
#define FALSE (*new variable((bool)false, boolean))
#define STRING(value) (*new variable((std::string)value, stringType))			//working #define STRING(value) (*new stringVariable(value))
#define NUMBER(value) (*new variable((double)value, number))				//working #define NUMBER(value) (*new numVariable(value))
#define JSON(ID)	;flag_reset();variable ID; ID						//working #define JSON(ID)	Variable ID; ID
#define TYPE_OF(ID) ID.get_type()
#define SIZE_OF(ID) ID.get_size()
#define IS_EMPTY(ID) ID.is_empty()
#define HAS_KEY(ID,key) ID.has_key(key)
#define ASSIGN =
#define SET ;flag_reset();
#define APPEND +=
#define PRINT  ;dummy_var &= 						//operator << overload
#define ERASE ;dummy_var |= 
#define PROGRAM_END ;return 1;}





PROGRAM_BEGIN

JSON(boolvar) = TRUE || FALSE
PRINT(boolvar)

JSON(var22) = (STRING("xexe") == STRING("xexe") == TRUE)
PRINT(var22)

JSON(var) = STRING("xexe") + STRING(" ok ok")
PRINT(var)
JSON(var3) = NUMBER(3) - NUMBER(4)
PRINT(var3)

SET var3 ASSIGN ARRAY[STRING("allagh se array"), NUMBER(99)]
//; cout << "here " << endl;
PRINT(var3)


JSON(pinakas2) = ARRAY[NUMBER(10), NUMBER(20), NUMBER(30), TRUE]
//; cout << "debug-- " << endl;
PRINT(pinakas2)

SET pinakas2 APPEND NUMBER(75), NUMBER(125)

PRINT(pinakas2)

ERASE pinakas2 

PRINT TYPE_OF(var)

PRINT pinakas2


//; cout << "pinakas2-- " << endl;
PRINT var, NUMBER(69), NUMBER(649)
//;
//var3 = NUMBER(11111), NUMBER(22222)


/*
obj2 = (*new variable(object)) = *new std::map < std::string, variable > {
//	{ true ? m["first"] = "true" : m["first"] = "false" }
{}, { KEY(ok) : NUMBER(3333333333) }, { "eeee", false ? var : var },
{ "kley3", NUMBER(3333333333) }
};
*/

JSON(objj) = OBJECT{ KEY(ok) : NUMBER(12), KEY(LEL) : FALSE }}
PRINT HAS_KEY(objj, "ok")
PRINT objj

SET objj APPEND OBJECT{KEY(a) : STRING("alpha")} }
PRINT objj

JSON(emptyObj) = OBJECT{}}
SET emptyObj ASSIGN OBJECT{KEY(B) : STRING("beta")} }
PRINT emptyObj

ERASE objj
PRINT objj


//JSON(pinakas2) = ARRAY[NUMBER(20), TRUE, NUMBER(30)]
//PRINT(pinakas2)

//JSON(pinakas12) = pinakas + pinakas2
//PRINT(pinakas12)


//JSON(kenos) = ARRAY
//PRINT(kenos)
//;

; cin.get();

PROGRAM_END