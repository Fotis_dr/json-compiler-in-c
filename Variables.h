#pragma once
#include <string>
#include <math.h>       /* fmod */
#include <map>
#include <vector>
#include <typeinfo> 
using namespace std;

enum Type{
	stringType,
	number,
	object,
	jarray,
	boolean,
	nil
};

string typeName[]{
		"string",
		"number",
		"object",
		"array",
		"boolean",
		"nil"
};

enum comma_type{
	comma_reset,
	comma_print,
	comma_array,
	comma_append
};

enum comma_type comma_type_flag;
//enum comma_type prev_comma_flag = comma_type_flag;

class variable;
vector<variable> global_array;
variable* to_be_inserted;
int print_flag;

void flag_reset(){
	comma_type_flag = comma_reset;
}

class variable
{
public:
	enum Type type;
	double d_value;
	std::string s_value;
	bool b_value;
	std::map <std::string, variable> m_value;
	std::vector<variable> a_value;


	variable(){};
	variable(enum Type new_var_type){
		//if (new_var_type == array) {
		comma_type_flag = comma_reset;	
		to_be_inserted = nullptr;
		//cout << "flag reset is "<<comma_type_flag  << endl;
		//}
	    this->type = new_var_type;
	};
	variable(double val,enum Type){ 
		this->type = number;
		this->d_value = val;
	};
	variable(bool val,enum Type){
		this->type = boolean;
		this->b_value = val;
	};
	variable(std::string val,enum Type){
		this->type = stringType;
		this->s_value = val;
	};

	//void printVar();
	void setValued(double val){ this->d_value = val; };
	double   getValued(){ return this->d_value; };


	variable operator+(variable b){
		variable newvar;
		//enum Type typ = number;
		//cout << " got here with values :"+this->svalue <<" - "+b.svalue << endl;
		if (this->type == number && b.type == number){
			newvar.setValued(this->getValued() + b.getValued());
			newvar.type = number;
			return newvar;
		}
		else if (this->type == stringType && b.type == stringType){
			newvar.s_value = (this->s_value + b.s_value);
			newvar.type = stringType;
			return newvar;
		}
		else if (this->type == jarray && b.type == jarray){
			newvar.a_value =(*this).a_value;
			newvar.type = jarray;
			for (int i = 0; i < b.a_value.size(); i++){
				newvar.a_value.push_back(b.a_value[i]);
			}
			return newvar;
		}
		else{
			cout << "Adding wrong types!" << endl;
			cin.get();
			exit(-1);
		}

	};
	variable operator-(variable b){
		variable newvar;
		//enum Type typ = number;
		//cout << " got here with values :"+this->svalue <<" - "+b.svalue << endl;
		if (this->type == number && b.type == number){
			newvar.setValued(this->getValued() - b.getValued());
			newvar.type = number;
			return newvar;
		}
		else{
			cout << "Subtracting wrong types!" << endl;
			cin.get();
			exit(-1);
		}

	};
	variable operator/(variable b){
		variable newvar;
		//enum Type typ = number;
		//cout << " got here with values :"+this->svalue <<" - "+b.svalue << endl;
		if (this->type == number && b.type == number){
			newvar.setValued(this->getValued() / b.getValued());
			newvar.type = number;
			return newvar;
		}
		else{
			cout << "Dividing wrong types!" << endl;
			cin.get();
			exit(-1);
		}

	};
	variable operator*(variable b){
		variable newvar;
		//enum Type typ = number;
		//cout << " got here with values :"+this->svalue <<" - "+b.svalue << endl;
		if (this->type == number && b.type == number){
			newvar.setValued(this->getValued() * b.getValued());
			newvar.type = number;
			return newvar;
		}
		else{
			cout << "Multiplying wrong types!" << endl;
			cin.get();
			exit(-1);
		}
	};

	variable operator%(variable b){
		variable newvar;
		//enum Type typ = number;
		//cout << " got here with values :"+this->svalue <<" - "+b.svalue << endl;
		if (this->type == number && b.type == number){
			newvar.setValued(fmod(this->getValued(),b.getValued()));
			newvar.type = number;
			return newvar;
		}
		else{
			cout << "Mod wrong types!" << endl;
			cin.get();
			exit(-1);
		}
	};
	variable operator<(variable b){
		variable newvar;
		//enum Type typ = number;
		//cout << " got here with values :"+this->svalue <<" - "+b.svalue << endl;
		if (this->type == number && b.type == number){
			newvar.b_value = (this->d_value < b.d_value);
			newvar.type = boolean;
			return newvar;
		}
		else{
			cout << "Comparing wrong types, can only compare numbers with >,<,<=,>= !" << endl;
			cin.get();
			exit(-1);
		}
	};
	variable operator>(variable b){
		variable newvar;
		//enum Type typ = number;
		//cout << " got here with values :"+this->svalue <<" - "+b.svalue << endl;
		if (this->type == number && b.type == number){
			newvar.b_value = (this->d_value>b.d_value);
			newvar.type = boolean;
			return newvar;
		}
		else{
			cout << "Comparing wrong types, can only compare numbers >,<,<=,>= !" << endl;
			cin.get();
			exit(-1);
		}
	};
	variable operator<=(variable b){
		variable newvar;
		//enum Type typ = number;
		//cout << " got here with values :"+this->svalue <<" - "+b.svalue << endl;
		if (this->type == number && b.type == number){
			newvar.b_value = (this->d_value <= b.d_value);
			newvar.type = boolean;
			return newvar;
		}
		else{
			cout << "Comparing wrong types, can only compare numbers >,<,<=,>= !" << endl;
			cin.get();
			exit(-1);
		}
	};
	variable operator>=(variable b){
		variable newvar;
		//enum Type typ = number;
		//cout << " got here with values :"+this->svalue <<" - "+b.svalue << endl;
		if (this->type == number && b.type == number){
			newvar.b_value = (this->d_value >= b.d_value);
			newvar.type = boolean;
			return newvar;
		}
		else{
			cout << "Comparing wrong types, can only compare numbers >,<,<=,>= !" << endl;
			cin.get();
			exit(-1);
		}
	};
	variable operator&&(variable b){
		variable newvar;
		//enum Type typ = number;
		//cout << " got here with values :"+this->svalue <<" - "+b.svalue << endl;
		if (this->type == boolean && b.type == boolean){
			newvar.b_value = (this->b_value && b.b_value);
			newvar.type = boolean;
			return newvar;
		}
		else{
			cout << "Comparing wrong types, can only compare boolean variables with (&&,||,!) !" 
				<< endl;
			cin.get();
			exit(-1);
		}
	};
	variable operator||(variable b){
		variable newvar;
		//enum Type typ = number;
		//cout << " got here with values :"+this->svalue <<" - "+b.svalue << endl;
		if (this->type == boolean && b.type == boolean){
			newvar.b_value = (this->b_value || b.b_value);
			newvar.type = boolean;
			return newvar;
		}
		else{
			cout << "Comparing wrong types, can only compare boolean variables with (&&,||,!) !" 
				<< endl;
			cin.get();
			exit(-1);
		}
	};
	variable operator!(){
		variable newvar;
		//enum Type typ = number;
		//cout << " got here with values :"+this->svalue <<" - "+b.svalue << endl;
		if (this->type == boolean){
			newvar.b_value = !(this->b_value);
			newvar.type = boolean;
			return newvar;
		}
		else{
			cout << "Comparing wrong types, can only compare boolean variables with (&&,||,!) !"
				<< endl;
			cin.get();
			exit(-1);
		}
	};
	variable operator==(variable b){
		variable newvar;
		//enum Type typ = number;
		//cout << " got here with values :"+this->svalue <<" - "+b.svalue << endl;
		if (this->type == b.type){
			if (this->type == number){
				newvar.b_value = (this->d_value == b.d_value);
				newvar.type = boolean;
				return newvar;
			}
			else if(this->type==stringType){
				newvar.b_value = (this->s_value == b.s_value);
				newvar.type = boolean;
				return newvar;
			}
			else if (this->type == boolean){
				newvar.b_value = (this->b_value == b.b_value);
				newvar.type = boolean;
				return newvar;
			}
			else if (this->type == jarray){
				int this_size = this->a_value.size();
				int bsize = b.a_value.size();
				if (this_size != bsize){
					newvar.b_value = false;
					newvar.type = boolean;
					return newvar;
				}
				for (int i = 0; i < b.a_value.size(); i++){
					variable temp;
					temp.type = boolean;
					if ((*this).a_value[i].type != b.a_value[i].type){
						newvar.b_value = false;
						newvar.type = boolean;
						return newvar;
					}
					temp = (*this).a_value[i] != b.a_value[i];
					if (temp.b_value == true){
						newvar.b_value = false;
						newvar.type = boolean;
						return newvar;
					}
				}
				newvar.b_value = true;
				newvar.type = boolean;
				return newvar;
			}
		}
		else{
			cout << "Comparing wrong types, can only compare boolean variables with (&&,||,!) !" 
				<< endl;
			cin.get();
			exit(-1);
		}
	};
	variable operator!=(variable b){
		variable newvar;
		//enum Type typ = number;
		//cout << " got here with values :"+this->svalue <<" - "+b.svalue << endl;
		if (this->type == b.type){
			if (this->type == number){
				newvar.b_value = (this->d_value != b.d_value);
				newvar.type = boolean;
				return newvar;
			}
			else if (this->type == stringType){
				newvar.b_value = (this->s_value != b.s_value);
				newvar.type = boolean;
				return newvar;
			}
			else if (this->type == boolean){
				newvar.b_value = (this->b_value != b.b_value);
				newvar.type = boolean;
				return newvar;
			}
			else if (this->type == jarray){
				int this_size = this->a_value.size();
				int bsize = b.a_value.size();
				if (this_size == bsize){
					newvar.b_value = true;
					newvar.type = boolean;
					return newvar;
				}
				for (int i = 0; i < b.a_value.size(); i++){
					variable temp;
					temp.type = boolean;
					if ((*this).a_value[i].type != b.a_value[i].type){
						newvar.b_value = true;
						newvar.type = boolean;
						return newvar;
					}
					temp = (*this).a_value[i] == b.a_value[i];
					if (temp.b_value == false){
						newvar.b_value = true;
						newvar.type = boolean;
						return newvar;
					}
				}
				newvar.b_value = false;
				newvar.type = boolean;
				return newvar;
			}

		}
		else{
			cout << "Comparing wrong types, can only compare boolean variables with (&&,||,!) !" 
				<< endl;
			cin.get();
			exit(-1);
		}
	};
	// PRINT
	friend std::ostream& operator<<(std::ostream& stream,  variable b) {
		if (b.type == stringType){
			stream << b.s_value << endl;
			return stream;
		}
		else if (b.type == number){
			stream << b.d_value << endl;
			return stream;
		}
		else if (b.type == boolean){
			stream << std::boolalpha << b.b_value << endl;
			return stream;
		}
		else if (b.type == jarray){
			cout << endl;
			for (int i = 0; i < b.a_value.size(); i++){
				cout << b.a_value[i];
			}
			return stream;
		}
		else if (b.type == object){
			cout << endl;
			for (auto elem : b.m_value)
			{
				if (elem.first != "" )
					//cout << "type " << elem.second.get_type() << endl;
					cout << "Key : "<< elem.first << ", Value :  " << elem.second << endl;
			}
			return stream;
		}
		else{ //debug
			stream << "den exeis print giauton ton tupo" << endl;
			return stream;
		}
	};

	variable get_size(){
		variable return_var;
		return_var.type = number;
		if (this->type == jarray){
			return_var.d_value = (*this).a_value.size();
			return return_var;
		}
		else if (this->type == object){
			return_var.d_value = (*this).m_value.size();
			return return_var;
		}
		else{
			return_var.d_value = 1;
			return return_var;
		}
	};

	variable get_type(){
		variable return_var;
		return_var.type = stringType;
		return_var.s_value = typeName[this->type];
		return return_var;
	};

	variable is_empty(){
		variable return_var;
		return_var.type = boolean;
		return_var.b_value = false;
		if (this->type == jarray){
			if (!this->a_value.empty()){
				return_var.b_value = true;
			}
			return return_var;
		}
		else if (this->type == object){
			if (!this->m_value.empty()){
				return_var.b_value = true;
			}
			return return_var;
		}
		else{
			return return_var;
		}
	}

	variable has_key(std::string key){
		variable return_var;
		return_var.type = boolean;
		return_var.b_value = false;

		if (this->type == object){
			if (this->m_value.count(key) > 0) return_var.b_value = true;	
		}
	    return return_var;
	}

	variable operator=(std::map <std::string, variable> input_map){
		this->m_value = input_map;
		return *this;
	};

	
	variable operator,(variable b){
		//cout << "this has val : " << (*this) << endl;
		//cout << "comma with variable value : " << b << endl;
		if (comma_type_flag == comma_append){
			if (to_be_inserted == nullptr){
				cout << "to array gia append einai null"
					<< endl;
				cin.get();
				exit(-1);
			}
			to_be_inserted->a_value.push_back(b);
		}
		else if (comma_type_flag == comma_print){
			if (print_flag == 1) cout << *this << endl;
			print_flag = 0;
			cout <<", " << b << endl;
		}
		else{
			global_array.push_back(b);
		}
		return *this;
	};

	variable operator[](variable b){
		variable newvar;
		//cout << "operator with [variable/variables] called value : " << b << endl;
		
		//comma_type_flag = comma_reset;

		/* insert b sthn arxh epeidh einai to prwto stoixeio tou array */
		global_array.insert(global_array.begin(), b);
		
		if (!(this->a_value).empty()){  
			this->a_value.clear();
		}

		for (int i = 0; i < global_array.size(); i++){
			this->a_value.push_back(global_array[i]);
		}

		global_array.clear();
		
		//comma_type_flag = prev_comma_flag;
		return *this;
	};

	variable operator+=(variable b){   //append
		if (b.type == object && this->type == object){
			for (auto elem : b.m_value)
			{
				this->m_value[elem.first] = elem.second;
			}
		}
		else{
			global_array.insert(global_array.begin(), b);

			to_be_inserted = this;
			comma_type_flag = comma_append;

			for (int i = 0; i < global_array.size(); i++){
				this->a_value.push_back(global_array[i]);
			}

			global_array.clear();
		}
		return *this;
	};

	variable operator&=(variable b){   //print
		print_flag = 1;
		comma_type_flag = comma_print;
		cout << b << endl;
		return *this;
	};

	variable operator|=(variable &b){   //erase
		if (b.type == jarray){
			b.a_value.clear();
		}
		else if (b.type == object){
			b.m_value.clear();
		}
		else{
			b.type = nil; 
		}
		return *this;
	};


	variable operator[](std::string  key){
		variable not_found;
		not_found.type = nil; 
		if (this->m_value.count(key) > 0 && this->m_value[key].type != nil) return this->m_value[key];
		return not_found;
	};

	//Variable& operator=(Variable& b);
	~variable(){};
	
};
